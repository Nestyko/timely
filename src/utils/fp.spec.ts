import { zip } from './fp'

describe('zip', () => {
  const xs = [1, 2, 3, 4, 5, 6]
  const ys = ['a', 'b', 'c', 'd', 'e', 'f']
  const zs = [0xa, 0xb, 0xc, 0xd, 0xe, 0xf]

  it('should combine 2 arrays into one', () => {
    expect(zip([xs, ys])).toEqual([
      [1, 'a'],
      [2, 'b'],
      [3, 'c'],
      [4, 'd'],
      [5, 'e'],
      [6, 'f'],
    ])
  })

  it('should combine 3 different arrays into one', () => {
    expect(zip([xs, ys, zs])).toEqual([
      [1, 'a', 0xa],
      [2, 'b', 0xb],
      [3, 'c', 0xc],
      [4, 'd', 0xd],
      [5, 'e', 0xe],
      [6, 'f', 0xf],
    ])
  })
})
