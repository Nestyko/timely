import { DateTime } from 'luxon'
import {
  getTimeRange,
  getTimezones,
  isCurrentTime,
  getClosestTime,
} from './dates'

describe('dates utils', () => {
  it('should return a dateTimeRange based on a timezone', () => {
    const jsDate = new Date(
      'Thu Feb 13 2020 20:49:40 GMT-0300 (Chile Summer Time)'
    )
    const date = DateTime.fromISO(jsDate.toISOString())
    const result = getTimeRange(date)
    expect(result).toHaveLength(24 * 2)
    const firstTimeMinusOne = result[0].minus({ minute: 30 })
    const lastTime = result[result.length - 1]
    expect(firstTimeMinusOne.toFormat('HH:mm')).toEqual(
      lastTime.toFormat('HH:mm')
    )
  })

  it('should return all the timezones, just trying some', () => {
    const timezones = getTimezones()
    expect(timezones).toContain('America/Caracas')
    expect(timezones).toContain('America/Santiago')
    expect(timezones).toContain('Europe/Madrid')
  })

  it('should return the closests time', () => {
    const jsDate = new Date(
      'Thu Feb 13 2020 20:49:40 GMT-0300 (Chile Summer Time)'
    )
    const now = DateTime.fromISO(jsDate.toISOString())
    const closests = now.minus({ minutes: 15 })
    const timeRange = [
      now.minus({ minutes: 45 }),
      now.minus({ minutes: 35 }),
      now.minus({ minutes: 25 }),
      closests,
      now.plus({ minutes: 45 }),
      now.plus({ minutes: 55 }),
    ]
    expect(getClosestTime(now, timeRange)).toEqual(closests)
  })
})
