import { useState, useEffect } from 'react'
import { DateTime } from 'luxon'

export const useTimer: () => DateTime = () => {
  const [now, setNow] = useState(DateTime.local())
  useEffect(() => {
    let timer = setInterval(() => tick(), 1000)
    return () => clearInterval(timer)
  }, [])
  const tick = () => setNow(DateTime.local())
  return now
}
