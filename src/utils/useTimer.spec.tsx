import React, { FunctionComponent } from 'react'
import { mount } from 'enzyme'
import { DateTime } from 'luxon'
import { useTimer } from './useTimer'

jest.useRealTimers()

describe('<Clock />', () => {
  const ComponentUsingNow: FunctionComponent<any> = () => {
    const timer = useTimer()
    return <div> {timer.get('second')}</div>
  }
  it('should wrap a component and inject the now DateTime each second', done => {
    jest.spyOn(global, 'clearInterval')

    const component = mount(<ComponentUsingNow />)
    const before = parseInt(component.text(), 10)
    setTimeout(() => {
      const after = parseInt(component.text(), 10)
      const beforeDate = DateTime.local()
        .set({ second: before })
        .plus({ second: 1 })
      expect(beforeDate.get('second')).toEqual(after)
      expect(clearInterval).not.toHaveBeenCalled()
      component.unmount()
      expect(clearInterval).toHaveBeenCalled()
      done()
    }, 1100)
  })
})
