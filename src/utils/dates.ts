import { DateTime } from 'luxon'
import tzdata from 'tzdata'
import memoize from 'memoizee'

const _getTimezones = () =>
  Object.entries(tzdata.zones)
    .filter(([, v]) => Array.isArray(v))
    .map(([zoneName]) => zoneName)
    .filter(tz => DateTime.local().setZone(tz).isValid)

export const getTimezones = memoize(_getTimezones)

export const getTimeRange = (date: DateTime) => {
  const newDate = date.set({ minute: 0 })
  let range: Array<number> = []
  for (let i = -24; i < 48 - 24; i++) {
    range = range.concat(i)
  }
  return range.map(counter => newDate.plus({ minutes: counter * 30 }))
}

export const getClosestTime: (y: DateTime, xs: Array<DateTime>) => DateTime = (
  y,
  xs
) =>
  xs
    .map(dt => ({ dt, diff: Math.abs(y.diff(dt).as('seconds')) }))
    .reduce((acc, z) => (acc.diff < z.diff ? acc : z), {
      dt: DateTime.local(),
      diff: Infinity,
    }).dt
