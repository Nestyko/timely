export const zip: (args: Array<Array<any>>) => Array<any> = args => {
  let result: Array<Array<any>> = []
  for (let i = 0; i < args.length; i++) {
    const xs = args[i]
    for (let j = 0; j < xs.length; j++) {
      result[j] = result[j] ? result[j].concat(xs[j]) : [].concat(xs[j])
    }
  }
  return result
}
