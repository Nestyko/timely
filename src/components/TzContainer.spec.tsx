import React from 'react'
import { shallow } from 'enzyme'
import { getTimeRange } from '../utils/dates'
import { DateTime } from 'luxon'
import { TzContainer } from './TzContainer'

describe('<TzContainer />', () => {
  const jsDate = new Date(
    'Thu Feb 13 2020 20:49:40 GMT-0300 (Chile Summer Time)'
  )
  const date = DateTime.fromISO(jsDate.toISOString())

  it('should render one container with a row for each timezone and columns with the hours', () => {
    const timeRanges = [
      getTimeRange(date.setZone('America/Santiago')),
      getTimeRange(date.setZone('Asia/Tokyo')),
    ]
    const component = shallow(
      <TzContainer now={date} timeRanges={timeRanges} />
    )
    expect(component.find('.container-tz').length).toEqual(1)
    expect(component.find('Column').length).toEqual(48)
    expect(
      component.find('Column').filterWhere(c => c.props().active === true)
        .length
    ).toEqual(1)
    expect(
      component
        .find('Column')
        .first()
        .props().times.length
    ).toEqual(2)
  })
})
