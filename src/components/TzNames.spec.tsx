import React from 'react'
import { shallow } from 'enzyme'
import { TzNames } from './TzNames'

describe('<TzNames />', () => {
  const timezones = ['America/Caracas', 'Europe/Paris']

  it('should render the Tz names', () => {
    const component = shallow(
      <TzNames timezones={timezones} onRemove={() => {}} />
    )
    expect(component.text()).toMatch(/America\/Caracas/)
    expect(component.text()).toMatch(/Europe\/Paris/)
  })
  it('should call close function with correct param when clicked the close button', () => {
    const removeFn = jest.fn()
    const component = shallow(
      <TzNames timezones={timezones} onRemove={removeFn} />
    )
    component
      .find({ 'data-test-id': 'remove-btn' })
      .first()
      .simulate('click')
    expect(removeFn).toHaveBeenCalledWith(timezones[0])
  })
})
