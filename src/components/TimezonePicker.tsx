import React, { FunctionComponent } from 'react'
import { getTimezones } from '../utils/dates'
import Select, { ValueType } from 'react-select'
import './TimezonePicker.css'

type TimezonePickerProps = {
  onChange: (value: string) => void
  className?: string
  excludeTimezones?: Array<string>
}

type OptionType = { label: string; value: string }

export const TimezonePicker: FunctionComponent<TimezonePickerProps> = ({
  onChange,
  className,
  excludeTimezones = [],
}) => {
  const timezoneOptions = getTimezones()
    .filter(tz => !excludeTimezones.includes(tz))
    .map((tz: string) => ({
      label: tz,
      value: tz,
    }))
  return (
    <Select
      title="Timezones"
      className="tz-picker"
      options={timezoneOptions}
      value={{ label: 'Add Timezone', value: 'placeholder' }}
      isSearchable
      isMulti={false}
      onChange={(selectedOption: ValueType<OptionType>) => {
        if (selectedOption) {
          if (Array.isArray(selectedOption)) {
            throw new Error(
              'Unexpected type passed to ReactSelect onChange handler'
            )
          }
          const value = (selectedOption as OptionType).value
          onChange(value)
        }
      }}
      closeOnSelect
    />
  )
}
