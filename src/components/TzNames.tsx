import React, { FunctionComponent } from 'react'
import { useTimer } from '../utils/useTimer'
import { MdDelete } from 'react-icons/md'
import './TzNames.css'

type TzNamesProps = {
  timezones: Array<string>
  onRemove: (tz: string) => void
}

export const TzNames: FunctionComponent<TzNamesProps> = ({
  timezones,
  onRemove,
}) => {
  const timer = useTimer()
  return (
    <div className="names-tz">
      {timezones.map(tz => (
        <div className="name-container" key={tz}>
          <div
            className="danger-hover remove-btn"
            data-test-id="remove-btn"
            onClick={() => onRemove(tz)}
          >
            <MdDelete />
          </div>

          <span className="name">{tz}</span>
          <span className="clock">
            {timer.setZone(tz).toFormat('ccc HH:mm:ss')}
          </span>
        </div>
      ))}
    </div>
  )
}
