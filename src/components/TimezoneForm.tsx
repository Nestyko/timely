import React, { FunctionComponent } from 'react'
import { TimezonePicker } from './TimezonePicker'
import './TimezoneForm.css'

type TimezoneFormProps = {
  onSubmit: (timezone: string) => void
  className?: string
  excludeTimezones?: Array<string>
}

export const TimezoneForm: FunctionComponent<TimezoneFormProps> = ({
  onSubmit,
  className,
  excludeTimezones = [],
}) => {
  return (
    <div className={`form-tz ${className}`}>
      <TimezonePicker
        excludeTimezones={excludeTimezones}
        className="input-tz"
        onChange={onSubmit}
      />
    </div>
  )
}
