import React, { FunctionComponent, useEffect, useRef } from 'react'
import { DateTime } from 'luxon'
import { zip } from '../utils/fp'
import './TzContainer.css'
import { getClosestTime } from '../utils/dates'

type TzContainerProps = {
  timeRanges: Array<Array<DateTime>>
  now: DateTime
}

export const TzContainer: FunctionComponent<TzContainerProps> = ({
  timeRanges,
  now,
}) => {
  const containerRef = useRef<HTMLDivElement>(null)
  useEffect(() => {
    if (containerRef && containerRef.current) {
      const node = containerRef.current
      node.scrollTo({ left: (node.scrollWidth - node.clientWidth) * 0.53 })
    } else {
      console.error('Could not scroll to middle')
    }
  }, [])
  const timesByColumn = zip(timeRanges)
  const closestTime = getClosestTime(now, timeRanges[0])
  return (
    <div ref={containerRef} className="container-tz">
      {timesByColumn.map((times: Array<DateTime>) => (
        <Column
          active={closestTime.equals(times[0])}
          times={times}
          key={times.map(t => t.toISO()).join('')}
        />
      ))}
    </div>
  )
}

type ColumnProps = {
  times: Array<DateTime>
  active: boolean
}

const Column: FunctionComponent<ColumnProps> = ({ times, active }) => (
  <div className={`column ${active ? 'active' : ''}`}>
    {times.map(time => (
      <div key={`${time.zoneName} ${time.toISO()}`} className="row">
        {time.toFormat('HH:mm')}
      </div>
    ))}
  </div>
)
