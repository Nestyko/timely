import React from 'react'
import { DateTime } from 'luxon'
import { TimezoneForm } from './components/TimezoneForm'
import { getTimeRange } from './utils/dates'
import { TzContainer } from './components/TzContainer'
import { TzNames } from './components/TzNames'
import { useSyncedStorage } from 'mnemosyne-synced-storage'

import './App.css'

const App = () => {
  const defaultTimezones: Array<string> = [DateTime.local().zoneName]
  const [timezones, setTimezones] = useSyncedStorage({
    key: 'timezones',
    defaultValue: defaultTimezones,
  })
  const timeRanges = timezones
    .map(tz => DateTime.local().setZone(tz))
    .map(getTimeRange)
  const now = DateTime.local()
  return (
    <div className="App">
      <TimezoneForm
        excludeTimezones={timezones}
        onSubmit={tz => setTimezones([...timezones, tz])}
      />
      <div className="timezones">
        <TzNames
          timezones={timezones}
          onRemove={timezone =>
            setTimezones(timezones.filter(tz => tz !== timezone))
          }
        />
        {timezones.length > 0 && (
          <TzContainer now={now} timeRanges={timeRanges} />
        )}
      </div>
    </div>
  )
}

export default App
