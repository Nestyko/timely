import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import * as Sentry from '@sentry/browser'
import allsettled from 'promise.allsettled'
import ReactGA from 'react-ga'
import {
  MnemosyneProvider,
  urlParamsAdapter,
  MultiStorageAdapter,
  chromeStorageAdapter,
  localStorageAdapter,
} from 'mnemosyne-synced-storage'

if (process.env.NODE_ENV === 'production') {
  ReactGA.initialize('UA-161443747-1')
  if (!!window && !!window.location) {
    ReactGA.pageview(window.location.pathname + window.location.search)
  }
}

allsettled.shim()

Sentry.init({
  dsn: process.env.REACT_APP_SENTRY_DSN,
})

const storageAdapter = new MultiStorageAdapter([
  urlParamsAdapter(),
  chromeStorageAdapter(),
  localStorageAdapter(),
])

ReactDOM.render(
  <MnemosyneProvider adapter={storageAdapter}>
    <App />
  </MnemosyneProvider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
